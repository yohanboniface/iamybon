from hashlib import md5
from io import StringIO
from pathlib import Path

from invoke import task, run

NGINX_CONF = """
server {
    listen   80;
    server_name yohanboniface.me;
    root /srv/iamybon/build/;
    index index.html;
}
"""


def as_ybon(ctx, cmd, *args, **kwargs):
    ctx.run('sudo --set-home --preserve-env --user ybon {}'.format(cmd),
            *args, **kwargs)


def sudo_put(ctx, local, remote, chown=None):
    tmp = str(Path('/tmp') / md5(remote.encode()).hexdigest())
    ctx.put(local, tmp)
    ctx.run('sudo mv {} {}'.format(tmp, remote))
    if chown:
        ctx.run('sudo chown {} {}'.format(chown, remote))


def put_dir(ctx, local, remote):
    local = Path(local)
    remote = Path(remote)
    for path in local.rglob('*'):
        relative_path = path.relative_to(local)
        if path.is_dir():
            as_ybon(ctx, 'mkdir -p {}'.format(remote / relative_path))
        else:
            sudo_put(ctx, path, str(remote / relative_path))


@task
def system(ctx):
    ctx.run('sudo apt update')
    ctx.run('sudo apt install -y nginx')
    ctx.run('sudo mkdir -p /srv/iamybon')
    ctx.run('sudo chown ybon:users /srv/iamybon/')


@task
def http(ctx):
    sudo_put(ctx, StringIO(NGINX_CONF), '/etc/nginx/sites-enabled/iamybon',
             chown='ybon:users')
    ctx.run('sudo systemctl restart nginx')


@task
def bootstrap(ctx):
    system(ctx)
    http(ctx)


@task
def deploy(ctx):
    run('python iamybon/app.py build')
    put_dir(ctx, 'iamybon/build', '/srv/iamybon/build')
