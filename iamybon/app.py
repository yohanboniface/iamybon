#!/usr/bin/env python
"""
I am Ybon, my personal site.
Usage:
    app.py serve
    app.py static
    app.py build
    app.py fetch_books
"""
import os

from docopt import docopt
from flask_frozen import Freezer
from flask import Flask, render_template, send_from_directory, abort

from iamybon import reading

app = Flask(__name__)


@app.route("/")
def index():
    context = {
        'body_class': 'home',
        'title': 'Yohan Boniface - personal web page',
        'description': "Yohan Boniface's personal web page, web citizen",
    }
    return render_template('index.html', **context)


@app.route("/cv/")
def cv():
    context = {
        'body_class': 'cv',
        'title': 'Yohan Boniface - Curriculum Vitae',
        'description': "Yohan Boniface's Curriculum Vitae, web architect",
    }
    return render_template('cv.html', **context)


@app.route("/cv/fr/")
def cvfr():
    context = {
        'body_class': 'cv',
        'title': 'Yohan Boniface - Curriculum Vitae',
        'description': "Curriculum Vitae de Yohan Boniface, architect web",
    }
    return render_template('cv-fr.html', **context)


@app.route('/festival-des-libertes-2012/')
def festival_des_libertes_2012():
    return render_template('festival_des_libertes_2012.html')


@app.route('/curieuse/')
def curieuse():
    return render_template('curieuse.html')


@app.route('/rosalie/')
def rosalie():
    return render_template('rosalie.html')


@app.route('/mamie/')
def mamie():
    return render_template('mamie.html')


@app.route('/escloupes/')
def escloupes():
    return render_template('escloupes.html')


@app.route('/books/')
def books():
    return render_template('books.html', books=reading.books())


@app.route('/books/<id>/cover')
def book_cover(id):
    return send_from_directory('reading/data/cover', id)


@app.route('/books/<id>.html')
def book(id):
    book = reading.load(id)
    if not book:
        abort(404)
    return render_template('book.html', book=book)


if __name__ == '__main__':
    args = docopt(__doc__, version='0.0')

    freezer = Freezer(app)

    if args['serve']:
        app.run(
            debug=True,
            port=os.environ.get('PORT', 5445),
            host=os.environ.get('HOST', '0.0.0.0')
        )
    elif args['build']:
        freezer.freeze()
    elif args['fetch_books']:
        reading.fetch()
    elif args['static']:
        freezer.serve(
            debug=os.environ.get('DEBUG'),
            port=os.environ.get('PORT', 5445),
            host=os.environ.get('HOST', '0.0.0.0')
        )
