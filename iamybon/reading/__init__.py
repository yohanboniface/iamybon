import json
import re
from pathlib import Path
from urllib.request import urlretrieve

import ebooklib
import ebooklib.epub
from peewee import FloatField, IntegerField, Model, SqliteDatabase, TextField

SOURCE = Path('/var/run/media/ybon/KOBOeReader/')

database = SqliteDatabase(str(SOURCE / '.kobo/KoboReader.sqlite'))


class BaseModel(Model):
    class Meta:
        database = database


class Bookmark(BaseModel):
    bookmarkid = TextField(db_column='BookmarkID', primary_key=True)
    chapterprogress = FloatField(db_column='ChapterProgress')
    datecreated = TextField(db_column='DateCreated', null=True)
    text = TextField(db_column='Text', null=True)
    volumeid = TextField(db_column='VolumeID')

    def json(self):
        return {
            'progress': self.chapterprogress,
            'text': self.text,
        }

    class Meta:
        db_table = 'Bookmark'


class Content(BaseModel):
    attribution = TextField(db_column='Attribution', null=True)
    contentid = TextField(db_column='ContentID', primary_key=True)
    kind = TextField(db_column='ContentType')
    datelastread = TextField(db_column='DateLastRead', null=True)
    description = TextField(db_column='Description', null=True)
    isbn = TextField(db_column='ISBN', null=True)
    imageid = TextField(db_column='ImageId', null=True)
    language = TextField(db_column='Language', null=True)
    publisher = TextField(db_column='Publisher', null=True)
    # 0 unread | 1 started | 2 finished
    readstatus = IntegerField(db_column='ReadStatus', null=True)
    subtitle = TextField(db_column='Subtitle', null=True)
    title = TextField(db_column='Title', null=True)
    numpages = IntegerField(db_column='___NumPages', null=True)
    percentread = IntegerField(db_column='___PercentRead', null=True)

    def __str__(self):
        return self.title

    def json(self):
        return {
            'attribution': self.attribution,
            'lastread': self.datelastread,
            'description': self.description,
            'isbn': self.isbn,
            'language': self.language,
            'publisher': self.publisher,
            'subtitle': self.subtitle,
            'title': self.title,
            'cover': self.cover,
            'annotations': self.annotations,
            'id': self.filename,
        }

    @property
    def path(self):
        # if 'file:///' not in self.contentid:
        #     return SOURCE / '.kobo/kepub' / self.contentid
        return SOURCE / self.contentid.replace('file:///mnt/onboard/', '')

    @property
    def filename(self):
        if self.isbn and self.isbn.isdigit():
            filename = self.isbn
        else:
            filename = '-'.join([self.attribution, self.title])
            filename = SAFE_STRING.sub('-', filename)
        return filename

    def extract_annotations(self):
        self.annotations = []
        for annotation in (Bookmark.select()
                           .where(Bookmark.volumeid == self.contentid)
                           .where(Bookmark.text!= None)
                           .order_by('chapterprogress')):
            self.annotations.append(annotation.json())
        # Old API
        if not self.description:
            return
        for percent, text in ANNOTATION.findall(self.description):
            annotation = Bookmark()
            annotation.percent = float(percent) / 100
            annotation.text = text.strip()
            self.annotations.append(annotation.json())
        self.description = self.description.split('<div class="user_annotations">')[0]

    def extract_cover(self):
        self.cover = None
        destination = DESTINATION / 'cover' / self.filename
        self.cover = self.filename
        if destination.exists():
            return
        if not self.path.exists():
            src = input(f"Path does not exist, please provide an URL ({self})")
            urlretrieve(src, destination)
            return
        try:
            epub = ebooklib.epub.read_epub(str(self.path))
        except AttributeError:
            # See https://github.com/aerkalov/ebooklib/issues/105
            print('Error while fetching cover of', self.title)
        else:
            try:
                # Sometimes, the cover id is given in a <meta> tag…
                meta = epub.get_metadata(ebooklib.epub.NAMESPACES['OPF'],
                                         'cover')[0][1]['content']
            except (AttributeError, IndexError, KeyError):
                meta = None
            with Path(destination).open('wb') as f:
                covers = list(epub.get_items_of_type(ebooklib.ITEM_IMAGE))
                if covers:
                    for cover in covers:
                        if (meta and cover.id == meta) or 'cover' in cover.id:
                            break
                    else:
                        cover = covers[0]
                    f.write(cover.get_content())

    def extract(self):
        self.extract_cover()
        self.extract_annotations()
        filename = '{}.json'.format(self.filename)
        with DESTINATION.joinpath(filename).open('w') as f:
            f.write(json.dumps(self.json(), sort_keys=True, indent=2, ensure_ascii=False))

    class Meta:
        db_table = 'content'


DESTINATION = Path(__file__).parent / 'data'
SAFE_STRING = re.compile('[^\w]+')
ANNOTATION = re.compile('Chapter Progress:[^\d]+([\d\.]*).*Highlight:[^ ]*([^<]*)')  # noqa


def fetch():
    for content in Content.select().where(Content.kind == '6',
                                          Content.readstatus == 2):
        content.extract()


def books():
    return [json.loads(f.open().read())
            for f in sorted(DESTINATION.glob('*.json'))]


def load(name):
    path = DESTINATION / (name + '.json')
    if path.exists():
        with path.open() as f:
            return json.loads(f.read())
    return None
