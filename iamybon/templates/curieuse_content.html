<p>Un jour de novembre. Je commence à parler dans le vide. Le vide froid d’un micro noir et de ma solitude. Je parle et ma voix résonne, résonne dans le ciel, ce petit ciel qui est le mien. Ma voix résonne et le Soleil, qui l’entend, la répète à qui le voit. Et ma voix franchit les montagnes et les océans et les continents, portée par les vents jusqu’à toi, et là-bas, tout là-bas, dans le lointain de ton île, je sais que tu m’écoutes. Ici, maintenant. Je bavarde un peu et tu es un peu là. Tu m’écoutes un peu et je suis un peu là. Un peu.</p>
<p>Il fait automne. L’automne. Vous savez, l’automne, où les feuilles s’habillent de rouge, de marron et d’or, puis tombent dans un air de violon monotone ; où les amours s’en sont allées, les cœurs se traînent, les langueurs se font longues.</p>
<p>Les chansons tristes s’entassent sous les platanes et le cantonnier les ramasse à la pelle. Parfois, le vent, coquin, d’un souffle, éparpille les tas à peine formés. Alors, le cantonnier lève sa pelle vers le ciel et annonce : « Quelle affreuse saison ! » puis se remet à la tâche. Les feuilles tombent, le cantonnier les met en tas, le vent déboule, disperse les amas et fait tomber d’autres feuilles, le cantonnier refait ses tas. Ainsi, l’automne se passe.</p>
<p>Dans un souffle, le vent qui emportait les feuilles emmène aussi ma voix.</p>
<p>Je reste bouche bée.</p>
<p>Tourbillonnant parmi les grandes feuilles des platanes, je la suis du regard : monter, descendre, s’envoler presque, puis se faire écraser sur le sol. Je la vois qui sautille, poussée à ras de terre par les saccades du vent, virevoltant, précipitée, puis planant. Parfois, dans l’accalmie, une feuille se pose sur ma voix, et je l’observe qui se dégage pour s’exposer encore au courant d’air. Il ne tarde pas. Et le ballet reprend. Et ma voix à nouveau se fait promener par les soubresauts de l’air d’automne, et chaque fois, quand elle semble enfin prendre son envol, la bourrasque la rabat sur les cailloux. Je la vois, essoufflée, au sol, désespérer, se décourager, presque abandonner. Alors je donne de la voix pour lui redonner force, courage, confiance, et elle reprend la lutte. Une volée spectaculaire, un rase-mottes interminable, un léger volettement, puis plus rien, plus de vent, plus un souffle. Et ça recommence. C’est la danse d’automne, celle qui d’un même plaisir fait et défait.</p>
<p>Je me laisse envoûter par cette sarabande hors du temps, égarant mon attention dans son mouvement. Une bourrasque sans doute un peu plus forte et décidée emporte finalement ma voix au-dessus des arbres sans me laisser le temps de lui dire ni adieu ni bonne fortune. Je lui fais un discret signe de la main et, tandis qu’elle prend le large, au-delà du village, du château, je détourne les yeux puis, reprenant machinalement ma pelle et mon balai, je refais les tas défaits.</p>
<p>Eh ben ça y est, c’est parti ! pense ma voix en se découvrant voyageuse. Mon bagage n’est pas bien lourd, un peu fragile peut-être. Bien sûr ma voix sait où elle doit se rendre. Mais comment, ça, non. « Une île, ça se trouve. »</p>
<p>Elle n’a pas l’habitude du voyage. Du moins pas du voyage toute seule. Ce n’est pas courant une voix qui se promène seule. Une voix, c’est tellement lié à qui l’émet que si l’on en perçoit une on s’attend à voir son maître pas loin. Combien de voix dépassent les quelques mètres ? Une voix, c’est voué à mourir. À mourir avec la distance, le temps, les obstacles, les autres bruits. Une voix, c’est le présent. On ne s’attarde pas devant une voix. Au mieux, on la suit dans son devenir. Tant qu’elle parle, elle avance. Quand elle ponctue, elle devient passée. Une voix, ça nous échappe. Tout au plus on fait répéter, mais ce n’est plus la même chose. Un paysage, vous restez devant, il ne va pas partir ? Il est là. Une voix, quand on l’entend, elle n’est déjà plus. D’ailleurs, la mienne est partie.</p>
<p>Partie d’un coup de vent, partie pour de bon. Et le vent l’emmène avec lui. Tel est le sort d’une voix : dépendre des caprices de l’air. Beaucoup ont perdu leur voix à la vouloir lancer contre le vent. Des rois, des femmes en colère, des dons Quichotte. Ma voix, elle, est assez raisonnable pour profiter de ce courant d’air qui l’embarque au-dessus du village. Mais, de géographie, ma voix ne sait rien. Certes, quittant le village, elle a reconnu le décor. Elle passait par là le mardi pour aller au marché ou d’autres fois pour se rendre à la ville. Elle avait bien lancé quelques saluts par là. Et, dans ce château qu’elle a maintenant au-dessous d’elle, combien de journées ensoleillées à écouter l’histoire de ses pierres ? Et tous ces cris qu’elle a lancés depuis cette rivière, quand on s’y baignait ? Certes, elle a reconnu le décor, mais est-ce, là, la direction de l’île fameuse ?</p>
<p>Dans ce courant aérien, ma voix a de nombreux compagnons de route. Quelques papillons un peu paniqués par l’altitude, quantité de pollens, fils d’araignée égarés, quelque oiseau qui lui coupe la route, un sac plastique blanc, des bruits de tracteur ou tronçonneuse, mais qui ne restent guerre ; un ou deux coups de fusil, qui la doublent et s’en vont mourir plus loin. Et leur écho qui revient s’échouer contre le vent. Beaucoup de poussière aussi.</p>
<p>Il est d’usage, dans les airs, de se saluer par une voltige. Et c’est ce qui rend si sautillantes ces étoiles blanches qui filent dans le ciel des belles journées : les révérences accumulées. Faites donc l’expérience. La prochaine fois qu’un de ces objets plus légers que l’air vous passe devant, lancez-lui un bruit, quelque chose, vous verrez s’il ne vous répondra pas d’une pirouette en l’air.</p>
<p>Ainsi, ma voix avance, poussée par un vent modéré et une folle mission. Elle avance d’une trajectoire bien peu rectiligne, enchaînant les courbettes aériennes, et, entre deux saluts, se demande si sa direction a quelque lien avec le but de son voyage.</p>
<p>De toute façon, pense-t-elle, si la Terre est bien ronde, toutes les directions mènent à toutes les destinations. S’agit de ne pas se choper le plus long chemin, voilà tout. Mais ce n’est pas rien.</p>
<p>« Excusez-moi, mon brave, hasarde ma voix à un papillon quand elle a fini de le saluer, savez-vous où nous allons ?</p>
<p>– Bien sûr, je vais dans cette prairie, là-bas. On dit que les fleurs y sont exquises.</p>
<p>– Ah ?… Mais… je voulais savoir où mène ce vent ?</p>
<p>– Vers là, je crois.</p>
<p>– Euh… oui, on dirait. Merci. »</p>
<p>Et ma voix avance, avance, sans savoir où elle va. Une alouette qui passait par-là, la voyant ainsi, tout à ses pensées, l’aborde en ces termes :</p>
<p>« Mais dis-moi, est-ce vraiment avancer ? Avances-tu si tes pas ne vont pas où tu portes l’esprit et le regard ? Il est tant de pierres et de terres pour y poser son pas. Tant de pierres glissantes et de terres boueuses. Il est tant de façons d’avancer et tant de façons de reculer. Et bien souvent elles sont les mêmes. Parfois, on avance à reculons ; parfois, un pas vers l’arrière mène vers l’avant. D’autres fois, c’est le pas qu’on n’a pas fait qui nous a fait avancer. Je sais, moi, avancer sans battre des ailes, ou même battre des ailes sans avancer. Et puis, un pas dans la mauvaise direction reste un pas, mais un pas dans le vide ? »</p>
<p>Ma voix va dans le vide en effet.</p>
<p>« Et quand on ne sait pas où aller, après tout, un pas dans le vide reste un pas ; mais quand on a un but ? »</p>
<p>Ma voix va dans le vide, et ça ne lui va pas du tout.</p>
<p>Mais où vais-je ? pense-t-elle, l’oiseau reparti.</p>
<p>Et puis il est drôle, lui, là, l’autre ; il parle, il parle, il parle, il veut que je voyage jusqu’à l’île, son île adorée, et il me lance sans la moindre indication sur la route. Je voudrais bien l’y voir, tiens, lui, lourd comme il est, là.</p>
<p>Et elle continue sa route, un peu agacée, un peu soucieuse. Cela dure un moment, où elle ne sait que faire et se laisse porter. Mais la voilà qui se lance dans une cabriole maladroite pour saluer un nouvel arrivant. Un bruit d’avion.</p>
<p>« Excusez-moi, lui dit ma voix un peu désespérée, je cherche ma direction, pourriez-vous m’aider ?</p>
<p>– Eh ! Oh ! Y a pas écrit « Agence de voyages », ici !</p>
<p>– S’il vous plaît… Savez-vous où va ce vent ?</p>
<p>– Bien sûr, il va à l’aéroport, comme tout le monde.</p>
<p>– Quel aéroport ?</p>
<p>– Je vous avouerais que je ne sais plus.</p>
<p>– Et vous, d’où venez-vous ?</p>
<p>– De l’avion.</p>
<p>– Évidemment, j’aurais dû m’en douter. Et où allez-vous alors ?</p>
<p>– Au sol, pour faire lever la tête aux hommes. Mais ça suffit maintenant, le devoir m’appelle, adieu !</p>
<p>Une telle indifférence laisse ma voix sans timbre. En plus, le vent s’estompe. Ma voix perd dangereusement de l’altitude. Si elle touche terre, elle peut renoncer à son voyage, elle le sait. Le sol se fait de plus en plus menaçant. Ma voix essaie de s’accrocher à l’air, elle souffle, elle aspire, elle appelle, elle hurle, elle braille, elle criaille, elle s’égosille, mais rien n’y fait. Elle se voit mourir lentement, prisonnière des hautes herbes qui l’attendent, denses comme une forêt tropicale, quand, d’un coup, le vent repart et prend ma voix dans ses bras.</p>
<p>« Ouf ! laisse échapper ma voix.</p>
<p>– De rien.</p>
<p>– Mais ! Qui parle ? s’étonne ma voix.</p>
<p>– Eh bien, c’est moi-même, le Vent. Et toi, qui es-tu ?</p>
<p>– Une voix.</p>
<p>– J’avais compris, merci. Mais que fait une voix, ici, perdue, sans son maître ?</p>
<p>– Eh bien je vais dans l’île rejoindre cette fille.</p>
<p>– Quelle île ? Quelle fille ?</p>
<p>– Euh.. je crois que l’île est Curieuse, et la fille ressemble à une île. Mais où va-t-on ? Il me semble reconnaître des endroits où je suis déjà passée…</p>
<p>– À t’entendre, j’ai l’impression que c’est la fille qui est curieuse. Et toi aussi. On ne peut retrouver une île juste en sachant qu’elle est curieuse. Selon qui l’on est, rien n’est curieux, et tout est curieux. Ce n’est pas un caractère très fiable… Ne sais-tu rien de plus ?</p>
<p>– Peut-être… Où va-t-on ?</p>
<p>– C’est à moi que tu le demandes ? Expliques-moi plutôt pourquoi tu cherches cette île et cette fille !</p>
<p>– J’ai quelques mots à leur porter… Mais, s’il te plaît, dis-moi où nous allons ! Pourquoi as-tu changé de direction tout à l’heure ? Et pourquoi t’es-tu arrêté pour repartir aussitôt ?</p>
<p>– Je suis le Vent, je te rappelle. Le Vent tourne comme bon lui semble et il n’a de raison à donner à personne. Tiens, regarde ! »</p>
<p>Le Vent fait un brusque virage sur le côté. Ma voix est toute retournée.</p>
<p>« Tu vois ?</p>
<p>– Oui, mais quel vent es-tu ? Tramontane ? Mistral ? Borée ? Noroît ? Harmattan ? Où prends-tu donc ta source et quel pays de ce monde t’accueille pour expirer dans ses montagnes ? Vois-tu la mer ou le désert ? Portes-tu la pluie ou le soleil ?</p>
<p>– Ah, ah, ah ! Rien de tout ça, ou plutôt si, je suis les vents, je suis le Vent. Celui qui lève les feuilles de l’automne et celui qui glace les neiges de l’hiver. Celui qui sèche le linge et celui qui fait claquer les volets. Je soulève les jupes et fait voler les chapeaux J’apporte les nuages et les emporte quand cela me plaît. Je suis la bise et le sirocco, la brise et l’ouragan. Je suis le Vent, tout simplement. Voilà bien une manie d’humain que celle de tout vouloir disséquer, classer, qualifier. Non, non, je suis le Vent, tout simplement.</p>
<p>– Tu veux dire qu’en même temps que tu souffles ici tu souffles aussi là-bas ?</p>
<p>– Bien entendu. Tiens, là, je soulève ses cheveux, délicatement. Je caresse ses pieds nus avec le sable blanc de l’île Curieuse. Elle est si belle.</p>
<p>– Les cheveux de la fille ? Ses pieds ? L’île Curieuse ? Je te parle ici et tu souffles là-bas ?</p>
<p>– Mais oui, puisque je te le dis.</p>
<p>– Mais alors, tu connais la fille, et l’île ?</p>
<p>– Moi, oui, bien sûr, je connais Curieuse. Je suis le Vent, et dis-moi en quel lieu le Vent ne soufflerait jamais ? Là-bas aussi j’ai soufflé, je souffle et je soufflerai encore. Oui, je connais cette île. C’est une île au milieu de l’océan, battue par les flots et entourées de récifs. Beaucoup ont perdu leur embarcation à la vouloir accoster. Sais-tu tous ces prétendants qui ont abîmé leur ego dans son corail en la voulant par trop approcher ? Oui, je connais cette île, moi, mais ça ne t’avance à rien.</p>
<p>– Mais si, tu peux me l’expliquer, toi, le chemin de l’île, si tu le connais.</p>
<p>– C’est que cette île, vraiment, est curieuse. Pour retrouver son chemin, tu dois perdre l’envie d’arriver. Aimer le voyage pour le voyage.</p>
<p>– Pour aller à cette île… je ne dois pas en avoir l’envie ?</p>
<p>– Aussi longtemps que tu la désireras, cette île se dérobera à toi.</p>
<p>– Mais comment une île peut-elle s’échapper ?</p>
<p>– Allons, ne me prends pas pour un imbécile ! Combien de fois l’as-tu vue se dérober dessous tes mains ardentes ? Combien de fois l’as-tu vue baisser le regard devant tes yeux accablants ?</p>
<p>– Mais de quoi parles-tu ? Je ne suis qu’une voix, moi…</p>
<p>– Encore ! Crois-tu que je suis né de la dernière pluie ? C’est moi-même qui la fait naître, la pluie…</p>
<p>– Dis-moi où tu m’emmènes, alors ! » insiste ma voix.</p>
<p>Mais le vent, comme il était arrivé, repart, d’un coup, et laisse tomber ma voix. Il lance, dernière attention pour elle :</p>
<p>« Tu veux retrouver le chemin de l’île ? Alors ne fais pas comme moi, qui file à travers le monde sans jamais m’y arrêter. Ne laisse pas tes yeux se perdre à l’horizon, regarde ton voyage se faire dessous tes pieds. »</p>
<p>Et voici ma voix qui chute de nouveau.</p>
<p>« Non, attends ! »</p>
<p>Elle s’apprêtait à maudire le Vent quand elle voit devant elle une immense étendue bleue.</p>
<p>« C’est la mer, mais oui, la mer. »</p>
<p>Ma voix se laisse envelopper par l’euphorie et ne cesse de s’exclamer :</p>
<p>« La mer ! Mon Dieu, voilà la mer ! La mer ! Merci, oh merci mon ami le Vent, voilà la mer ! Bientôt je verrai l’île ! La mer ! »</p>
<p>Même allégée par l’euphorie, ma voix finit par toucher le sol. Une plage de sable blanc. La mer y bat le temps de ses déferlantes. Et le temps avance, battu par les vagues. Le temps avance, battu par les vagues, et, à l’horizon, il couche le soleil : petit à petit, le voilà qui disparaît derrière les collines, tandis que la mer s’assombrit et ne reste que le bruit du temps dans les vagues. Le bruit des vagues sur la plage. Le bruit des vagues sur la plage.</p>
<p>Un petit somme s’empare alors de ma voix : elle s’étire, s’étend, s’allonge sur le sable et s’endort.</p>
<p>Quand elle s’éveille, la nuit l’a enveloppée et, avec elle, la mer et la terre. Le ciel est si frais et les étoiles scintillent par milliers au-dessus de la mer. Une douce musique a envahi les cieux : c’est le bavardage mélodieux des étoiles. Ma petite voix apprécie cet instant comme une naissance. Elle écoute. Le scintillement des étoiles, le bruit des vagues sur la plage. Elle écoute et savoure le moment.</p>
<p>« Eh bien, que fais-tu encore ici, n’es-tu pas en route ?</p>
<p>– Mais ! Qui parle ? demande ma voix.</p>
<p>– C’est moi, tu ne me vois pas ? Je suis la Mer.</p>
<p>– Je te vois, oui. Mais si je suis encore là, c’est que le Vent m’a laissée ici. Et comment sais-tu que la route m’attend ?</p>
<p>– Tu l’as dit au Vent, justement, et ce que le Vent sait, tout le monde finit par le savoir aussi : il est fiable comme un courant d’air !</p>
<p>– Saurais-tu alors me dire comment je dois faire pour rejoindre cette île ? En sais-tu le chemin ?</p>
<p>– Bien sûr. Comme toutes les îles, cette île, je la porte en mon sein. C’est moi qui la nourris le jour et moi qui la berce la nuit. Là, à cet instant, vois-tu, je caresse sa peau de toute mon eau, elle est si belle. Elle est si douce. Mais si périlleuse. Elle n’a pas de port d’accès. Pour y poser pied, il faut abandonner les vaisseaux d’apparat et embarquer dans quelque frêle barque qui, seule, pourra se frayer un passage à travers le corail qui l’enclave. Son chemin est la légèreté. Tu veux trouver le chemin de l’île ? Eh bien abandonne tes sabots, ta pelle, ton balai, les lourds habits dont tu as hérité, abandonne tout ce qui te rend plus lourde que l’eau salée.</p>
<p>– Comment ça ?</p>
<p>– Déleste-toi de ta charge si tu veux dépasser les écueils.</p>
<p>– Je ne comprends pas plus la direction que je dois prendre… Vraiment, je ne comprends pas…</p>
<p>– Tu sais, comprendre, ce n’est qu’apposer l’empreinte de ses propres limites, alors ne sois pas pressée de comprendre. Aussi, avant d’en chercher la direction, demande-toi si tu pourras accéder à cette île. Pas la peine de traverser océans, continents et montagnes pour aller ensuite t’échouer à ses pieds.</p>
<p>– Mais j’y accéderai par les airs !</p>
<p>– Ah, comme ta naïveté me plaît…</p>
<p>– Mais là, de quel côté dois-je partir, par là ou par là ? » demande ma voix, inquiète.</p>
<p>La plage n’offre que deux directions, et la Mer lui répond en ouvrant deux grands bras de vagues, un de chaque côté de ma voix :</p>
<p>« Par là ou par là, c’est pareil, si cette île est sur ton chemin, t’ai-je dit, tu ne pourras pas la manquer. Sois prête, plutôt ! »</p>
<p>Alors ma voix recommence à avancer, péniblement, sur le sable sec. Seul le bruit des vagues l’accompagne. Elle a beaucoup de mal à progresser. Il n’y a pas un souffle de vent qui soit là pour l’aider même un peu. La plage est sans fin. Et il fait encore très sombre. À nouveau, le rythme des vagues et la mélodie des étoiles. Ma voix avance comme un lamento. Reste le bruit des vagues sur la plage. Le bruit des vagues sur la plage. Le bruit des vagues sur la plage.</p>
<p>Ma voix progresse, progresse. Dans la bonne direction ? Elle ne le sait toujours pas. Elle avance le long de cette plage de sable, qui bientôt se transforme en roche. La progression y est toujours bien difficile pour une voix. Mais elle va de l’avant.</p>
<p>À force d’avancer, la fatigue à nouveau l’envahit. Elle s’arrête et s’assoupit quelque peu.</p>
<p>« Ouh ouh ! »</p>
<p>Quelqu’un l’appelle. Ma voix ouvre les yeux. Elle est émerveillée par le spectacle. La Lune est là et son reflet dans la mer vaut mille soleils. Comme elle est belle, la Lune.</p>
<p>« Ouh ouh ! »</p>
<p>C’est la lune qui appelle ma voix.</p>
<p>« Bonjour, madame la Lune, ou plutôt bonne nuit… »</p>
<p>Ma voix est tout impressionnée de parler à la Lune. Dame lune.</p>
<p>« Je parie que tu es la voix qui cherche cette fille qui est une île ? j’ai entendu le Vent qui parlait de toi…</p>
<p>– Ah, vous me connaissez ?</p>
<p>– Maintenant, tout le monde te connaît. Le Vent, c’est notre gazette. Il passe sont temps à babiller et jacasser. À colporter les nouvelles autour de la terre. »</p>
<p>Ma voix est éblouie par dame Lune.</p>
<p>« Vous êtes si belle madame la Lune. Et votre image dans la mer ne vous trahit pas d’un reflet. On dit que vous avez chez vous aussi des mers et des océans. Comme j’aimerais vous voir de tout près. Vous êtes si belle, madame la Lune.</p>
<p>– Mais tais-toi donc pauvre innocente ! Tu ne sais pas de quoi tu parles. Cette beauté que tu vantes est la plus atroce de mes souffrances. Elle est ce qui me cause le plus de maux et de tourments. Il n’est rien que je ne regrette plus. Et ne me parle pas de ce reflet sur l’eau qui est là pour me rappeler constamment l’origine de mon mal.</p>
<p>– Que dites-vous là, madame la Lune ?</p>
<p>– Sais-tu ce que signifie être condamnée, pour un vieux et banal péché d’orgueil, à ne montrer aux autres que le côté de soi qui est le plus admirable ? L’effeuillage que je fais au fil des nuits ne dévoile qu’une moitié de mon être. Qui verra jamais ma face cachée ? Est-ce que tu te rends compte de ça : je n’existe qu’à demi ? Et le plus dur, c’est que celle que vous ne voyez pas est précisément la face obscure de ma personnalité. C’est comme si on t’interdisait, à toi, de dire toute vulgarité, toute grossièreté, ce ne serait pas viable pour une voix, n’est-ce pas ?</p>
<p>– Non, c’est vrai. Mais j’ai peine à croire que votre beauté vous fait souffrir. Peine à croire que votre face cachée est moins céleste que celle que l’on voit.</p>
<p>– Pourtant, c’est ainsi. Pourquoi m’appelez-vous Lune ? C’est parce que vous ne voyez pas l’autre. Même quand vous me dites « pleine », je ne suis qu’à moitié. Les gens aiment une image, et son reflet, qui n’est pas moi. Ils regardent vers moi, mais saluent quelqu’un d’autre. Je me sens un imposteur.</p>
<p>– Ne soyez pas triste, madame la Lune. Et ne pleurez pas. »</p>
<p>Ma voix est tout émue.</p>
<p>« Ou plutôt si, pleurez, vos larmes vous rendent plus belle encore, et si différente. Vos larmes vous rendent une… une… euh… une, oui, vos larmes vous rendent une.</p>
<p>» Madame la Lune, je vous crois. Je vous crois.</p>
<p>– Merci, petite voix. »</p>
<p>Un long silence s’installe. La Lune monte dans le ciel et sèche ses larmes. Ma voix regarde la Mer. La Mer regarde ma voix. Et bat les rochers de ses vagues. Bat les rochers de ses vagues. Bat les rochers de ses vagues.</p>
<p>« Je l’ai vue, ton île, tout à l’heure. »</p>
<p>La Lune, haut dans la nuit, surprend à nouveau ma voix.</p>
<p>« Je l’ai vue, elle marchait, elle aussi, le long de la mer, ses cheveux faseyaient au vent et le sable caressait ses pieds. Je l’ai vue, ton île, et je jalouse sa beauté, sa beauté tout entière.</p>
<p>– Sauriez-vous alors comment on la retrouve ? Savez-vous le chemin de l’île ?</p>
<p>– Tu veux retrouver l’île ? Accepte-toi comme tu es. Ou, mieux encore, montre tes reflets disgracieux : ils sont toi aussi. Montre tes boutons, tes épis, tes fausses notes, tes ongles noirs. Montre tes faiblesses, et tu y gagneras ton être. Cette partie de toi que tu renies, embrasse-la, assez fort pour te fondre en elle et devenir ce que tu es. Ne cache pas ta face lumineuse, non. Mais montre aussi ta face cachée, c’est le chemin de l’île.</p>
<p>– C’est à moi que vous dites ça ? Moi, une voix perdue si loin de son maître…</p>
<p>– Oui, c’est à toi.</p>
<p>– Mais d’où part ce chemin, alors ? Je n’y comprends vraiment rien…</p>
<p>– Il ne reste pas grand-chose de ce qu’on a compris. Aussi, ne t’entête pas à comprendre. Suis ton chemin.</p>
<p>– Et vers où, alors ? Quel est ce chemin ? »</p>
<p>Mais la Lune garde le silence.</p>
<p>Ah, le chemin de l’île… Le chemin. Je ne suis pas bien avancée. Je ne sais toujours pas où je suis. Je ne sais toujours pas où je dois aller. Et puis l’autre, là, qui me lance sans rien me dire du chemin. Ah ça, c’est sûr, il est pas près de voler jusqu’à son île, lui, le lourd. Et moi ? Ma voix a dans le timbre les harmoniques du doute.« Mais existe-t-il seulement un chemin pour cette île ? »</p>
<p>« J’existe.</p>
<p>– Qui donc ?</p>
<p>– Moi, ton chemin, j’existe.</p>
<p>– …</p>
<p>– Je passe par là où tu vas. Ne me cherche pas au ciel, au loin, à l’horizon ou ailleurs, je suis là où tu poses le pied et le regard.</p>
<p>» Je suis de pierre et de terre et d’asphalte. Je suis aussi fait d’eau et parfois même d’air. Je suis un sentier de montagne et une autoroute pour la mer, de nuit. Je suis une route de campagne et un bateau pour l’île de tes ancêtres. Tu dis que tu me suis, mais c’est moi qui suis là où tes pas mènent. Ne me cherche pas, avance !</p>
<p>– Mais comment ça ?</p>
<p>– Où que tu ailles, je te dis, tu es sur le bon chemin, c’est-à-dire moi-même. Quand tu changes de direction, tu ne m’as pas quitté d’un mètre. Si tu reviens en arrière, tu ne m’as pas abandonné une seule seconde. Tu ne peux pas sortir de ton chemin, alors ne perds pas ton temps à le chercher. Avance !</p>
<p>– Mais vers où ?</p>
<p>– Là ou là, c’est pareil.</p>
<p>– Mais un côté mène sûrement plus vite à l’île que l’autre, non ?</p>
<p>– Peut être, mais il pourrait aussi y arriver trop vite. As-tu pensé à ça ?</p>
<p>– …</p>
<p>– Tu veux trouver le chemin de l’île ? Tu dois changer d’hémisphère, rappelle-toi ! Abandonne tes vieux réflexes ! »</p>
<p>La Lune intervient :</p>
<p>« As-tu pensé que, quand tu as la tête en haut, cette île de l’autre monde a la tête en bas, et, vice versa, si elle a la tête en haut, toi, c’est les pieds que tu as en haut ? À quoi te servirait d’arriver sur l’île si tu n’as pas fait l’effort de renverser ta façon de penser ? Qu’y ferais-tu à part voir tout à l’envers et ne rien comprendre ?</p>
<p>– Comme avant, si je puis me permettre. »</p>
<p>C’est la Mer qui vient de parler.</p>
<p>« Eh ! Oh ! Je ne suis qu’une voix, moi !</p>
<p>– Eh bien, voudras-tu prononcer les mots à l’envers de comme elle aime les entendre ? reprend la Lune. Dans cette vie, vois-tu, il y a deux mondes. Il y a le monde des uns et le monde des autres. C’est ainsi. Toi, dans quel monde es-tu ?</p>
<p>– Je ne sais pas dans quel monde je suis, avoue ma voix.</p>
<p>– Eh bien, poursuit la Lune, l’île est dans l’autre. L’as-tu compris ?</p>
<p>– Je ne suis qu’une voix…</p>
<p>– Mais cesse donc de faire l’innocente ! continue la Mer. N’étais-ce pas toi qui te refusait à lui parler pendant parfois des journées entières ? N’étais-tu pas porteur des mots qui l’ont blessée ? »</p>
<p>Ma voix, penaude, se fait petite.</p>
<p>« Allons, lui dit la Lune, ne sois pas honteuse, montre ta face cachée tant qu’il est encore temps, t’ai-je dit ! ou tu perdras l’île plus vite que tu ne l’auras retrouvée. Et tu n’y es pas encore. »</p>
<p>L’aube point très légèrement à l’horizon de la mer. Ma voix la regarde arriver en songeant aux larmes de la vie, les belles larmes de la vie. Celles qui font chatoyer le monde à nos yeux.</p>
<p>Une mouette qui dormait pas loin, réveillée par la conversation avec la Lune, s’approche de ma voix :</p>
<p>« Si tu veux, je vais vers le large, j’y ai une entrevue, je peux te prendre avec moi.</p>
<p>– Très volontiers », accepte ma voix en grimpant sur le dos de l’oiseau.</p>
<p>Enfin le voyage reprend. La mouette va vite, très vite, en effleurant la mer à toute vitesse. Ma voix a la vue toute mouillée de vie, parfois elle sent même les embruns et elle crie de joie, mais l’allure est telle qu’elle ne s’entend pas. Cela dure jusqu’à ce qu’ils perdent de vue la côte, en arrivant là où la mouette a son rendez-vous. À cet endroit, les vents de la haute mer soufflent régulièrement, et ce sont eux qui prennent le relais de la mouette en récupérant ma voix.</p>
<p>Alors l’allure se stabilise et le voyage gagne ainsi, peu à peu, sur l’infini bleu de la mer. Parfois, le bruit de quelque bateau de pêche vient rompre la monotonie. D’autres fois, ma voix croise le cri d’un oiseau marin, qui la salue, et elle lui répond de la révérence de rigueur.</p>
<p>Puis enfin arrive la terre. Et le vent poursuit dans sa direction. Il survole quelques villes toutes blanches, et, entre les villes, du jaune ocre et du marron. Puis il ne reste que le jaune ocre, et le Vent s’arrête. Net.</p>
<p>« Désolé, je te laisse ici. Si tu es encore là plus tard, je repasserai te prendre. »</p>
<p>Et il se retire.</p>
<p>« Mais où vas-tu ? Ne me laisse pas là en plan ! »</p>
<p>Le Vent est déjà parti. Reste le désert. Savez-vous ce qu’est le désert ? On dit qu’à part le sable il n’y a rien. Que c’est comme un tout fait de rien.</p>
<p>Personne à qui demander son chemin.</p>
<p>Si, le sable.</p>
<p>« Excuse-moi, j’aurais besoin d’un renseignement.</p>
<p>– Oui, fait le Sable. Je gage que tu cherches ta route ?</p>
<p>– Comment le sais-tu ?</p>
<p>– Avec l’accent que tu as, tu n’es pas d’ici. Et dans le désert, tout le temps, l’étranger cherche sa route. Pas un chemin tracé ici, tout est à refaire, toujours. Alors si tu veux trouver ton chemin adresse-toi au soleil. C’est le seul qui sait orienter les voyageurs du désert, du moins le jour. La nuit il y a les étoiles. Moi, ici, je ne vois que moi, alors je ne peux pas t’aider. »</p>
<p>Bien, ma voix n’a plus qu’à demander sa route au soleil.</p>
<p>« Pardon, là-haut, monsieur le Soleil, ohé !</p>
<p>– Je t’entends très bien, pas la peine de crier !</p>
<p>– Je cherche le chemin de l’île, pourriez-vous m’aider ?</p>
<p>– Je sais ce que tu cherches, le Vent m’a déjà parlé de toi. En fait, je n’entends parler que de toi depuis hier.</p>
<p>– Ah ? Et vous connaissez cette île ?</p>
<p>– Évidemment. Je suis le Soleil. Cette île, je la vois comme je te vois. Là, maintenant.</p>
<p>– Vous voyez l’île ?</p>
<p>– Je suis le Soleil, je te dis. Je vois l’île, oui, elle est allongée « au soleil », comme disent les humains ; tiens, d’ailleurs, elle regarde vers moi. Ah oui, oui, elle me regarde. Si elle savait que je te voyais aussi. Veux-tu que je lui dise quelque chose, que je lui fasse signe ?</p>
<p>– Non, non, ne la dérangez pas. Plutôt, racontez-moi encore. Que fait-elle ? Comment est-elle ? Est-elle belle comme le disent le Vent, la Lune, la Mer ?</p>
<p>– Elle est belle, oui. Ses cheveux, ses lèvres, ses mains, je vois tout.</p>
<p>– Racontez-moi !</p>
<p>– Elle est fraîche même sous le soleil. Elle est comme une gorgée d’eau de source quand on brûle au soleil. Je me réjouis de caresser prudemment sa douce peau de mes rayons. Elle est un astre sur l’océan. Elle est comme un lever de soleil permanent, une aube infinie. Je jalouse sa clarté et ses couleurs.</p>
<p>– Où est-elle alors ? Quel chemin y mène ? Est-elle encore loin ?</p>
<p>– Si tu veux trouver le chemin de l’île, tu dois chercher en toi la douceur et la délicatesse. Ne sois pas comme moi, qui brûle tout ce que j’approche, tant je ne sais respecter ce que je côtoie. Certes, je suis le père de la vie, et beaucoup me doivent tout. Mais il en est aussi qui n’ont pas besoin de moi pour vivre. Et même des pour qui j’apporte la mort, la neige par exemple. Je ne sais donner que la chaleur ou la lumière, et il est des êtres qui n’ont besoin ni de chaleur ni de lumière.</p>
<p>» Tu sais, quand je ne suis pas là, que vient la nuit, il y a les étoiles, non ? Il te semble à toi aussi, n’est-ce pas, que j’éclaire plus que mille étoiles réunies ? Eh bien, parmi ces étoiles, beaucoup sont des soleils mille fois plus gros que moi, et plus brillants et plus admirables, et pourtant tu les vois si petites. Dis-toi que moi, je ne les vois jamais. Tu entends ? Jamais ! Je n’ai pas comme toi la nuit pour les admirer, même petites. Et pourquoi je n’ai pas la nuit ? Parce que je suis là. Je ne peux pas m’éteindre. Entends-tu ? Je ne vois jamais briller mes semblables. Jamais.</p>
<p>» Veux-tu trouver le chemin de l’île ? Apprends d’abord à regarder briller tes semblables. »</p>
<p>Le soleil s’éclipse quelque peu pour laisser ma voix entendre ce qu’il vient de dire. Reste le désert, œuvre de l’excès de splendeur du soleil. Du sable comme une mer, avec des vagues jusqu’à l’horizon, et rien ni personne pour en troubler l’harmonie. Ma voix, avec quelque difficulté, commence à cheminer. Jamais encore sa direction ne lui était apparue si mystérieuse. Ni les conseils du Vent, ni ceux de la Lune, de la Mer ou du Soleil ne lui semblent utiles. Elle n’a que du sable devant elle, derrière, et de chaque côté. Elle chemine.</p>
<p>Bientôt, elle aperçoit de la poussière qui vole en haut d’une grande dune, et le sol tremble un peu. Ma voix s’arrête. Elle entend des pas. C’est une caravane de chameaux. Dans leur vacarme, ils ne remarquent pas ma voix et passent sans même s’arrêter. Heureusement, un animal est un peu à la traîne et, lui, l’aperçoit.</p>
<p>« Veux-tu que je te dépose quelque part ?</p>
<p>– Oh oui… dit-elle tout bas, c’est-à-dire que… » mais le chameau l’interrompt :</p>
<p>« Je sais ton histoire, déjà. Allons. »</p>
<p>Et ma voix se retrouve à dos de chameau.</p>
<p>« Sauriez-vous peut-être la direction de mon île ?</p>
<p>– Non, mais elle n’est sûrement pas par ici. Les îles, ici, sont de l’eau au milieu du sable. »</p>
<p>Ils marchent longtemps, dans le silence de leurs pas sur le sable. Puis l’océan de sable aboutit à une île de verdure, de vie, d’eau.</p>
<p>« Notre caravane s’arrête là pour ce soir, dit le chameau, si tu veux rester avec nous, demain nous pourrons te rapprocher encore un peu de la mer.</p>
<p>» Demain, la route sera longue. Alors repose-toi et profite de cet îlot. »</p>
<p>Ce que ma voix fait de bon cœur.</p>
<p>Et c’est ainsi qu’un nouveau jour se lève sur le voyage de ma voix.</p>
<p>Il faut encore affronter le désert. De sable d’abord, puis de roche ; puis de sable à nouveau. Dans ce qui lui semblait monotone, ma voix distingue maintenant quelques traces de vie. « Mais, pense-t-elle, pas de feuilles à ramasser en automne, ici. »</p>
<p>La caravane avance à son allure tranquille, quand une légère brise se lève. Aussitôt, ma voix reconnaît son ami le Vent et se laisse embarquer dans le courant aérien, profitant de cette nouvelle aubaine.</p>
<p>Bientôt, le désert est derrière eux. À présent, des grands plateaux, où paissent des zèbres, des girafes, des gazelles, peut-être des antilopes (ma voix ne connaît que très mal la faune exotique). Tiens ! Des montagnes enneigées, un peu de nostalgie. De grands lacs, de grands fleuves, puis, à nouveau, la mer.</p>
<p>Ma voix est heureuse de retrouver la Mer, mais les salutations sont brèves.</p>
<p>« Bah ! Il passera bien une autre mouette pour t’emmener… s’exclame la Mer. Fais-tu bon voyage ?</p>
<p>– Plutôt, oui ! Cette odyssée commence vraiment à être agréable. Il y a des personnes que je rencontre, des lieux que je traverse, je crois les connaître depuis toujours. Il y a quelque chose de familier dans ce monde…</p>
<p>– Alors tu approches…</p>
<p>– Des fois, je ne sais plus si je suis celui qui passe ou bien si je suis celui qui est là et qui regarde passer. Celui qui demande ou celui qui répond. Et puis, vois-tu, quand je suis portée par mon ami le Vent, je gagne en temps, certes, mais il me semble perdre en voyage. »</p>
<p>Ma voix se prête toujours bien volontiers à la discussion, la Mer aussi, alors elles s’étendent à bavarder, et une petite vague de temps passe de cette façon. Quelques mouettes ou goélands passent et partent au large. On ne pense même pas à leur demander quelque place pour ma voix. On papote.</p>
<p>« Bonjour. »</p>
<p>Une tortue s’est approchée.</p>
<p>Elle se présente.</p>
<p>On se présente.</p>
<p>« Ah, au fait, j’ai encore du chemin à faire, moi, s’inquiète ma voix.</p>
<p>– Mais j’y vais, justement, lui annonce la Tortue.</p>
<p>– Où donc ? s’étonne ma voix.</p>
<p>– Eh bien… bredouille la Tortue en montrant l’horizon. Je vais pas là, n’est-ce pas ta route ?</p>
<p>– C’est possible, oui, que mon chemin passe par l’horizon. Je te suis. »</p>
<p>Et ma voix s’installe sur la Tortue, qui commence à nager à la surface.</p>
<p>La conversation reprend.</p>
<p>Après bien des arguments évoqués, et tandis que l’on vogue en pleine mer, la Tortue dit à ma voix :</p>
<p>« Quelque chose m’intrigue. Je vois bien et je sais bien que tu n’es pas une tortue, et pourtant tu as une carapace plus grosse que la mienne. De quelle espèce es-tu ?</p>
<p>– Je ne sais pas, je ne comprends pas. Je ne me connais pas de carapace, s’étonne ma voix.</p>
<p>– Pourtant je vois bien que, pour atteindre le cœur de ta chair, il faut traverser une épaisseur des plus redoutable. Certes, tu es bien défendue, et tu ne seras pas la proie du premier prédateur venu !</p>
<p>– Mais enfin ! De quoi parles-tu ? Je n’ai pas de carapace, je n’ai rien qui m’entoure, qui me défende… Vraiment, je ne comprends pas !</p>
<p>– Allons, ne remarques-tu pas ces couches superflues qui s’entremettent dans tes relations aux autres ?</p>
<p>– Mais je ne suis qu’une voix, moi…</p>
<p>– En es-tu bien sûre ?</p>
<p>» Ah ! On s’imagine bien volontiers immortel quand, comme moi ou comme l’oursin, on s’enveloppe d’une protection radicale contre le monde. On croit qu’on survivra à toutes les épreuves, on se pense invincible et l’on brave la vie et la mort. Mais que peut-on craindre de la mort quand on a pas même vécu ?</p>
<p>» Seul qui se noiera dans la vie vivra.</p>
<p>» Et rappelle-toi ceci : plus on se défend par des artifices, plus on s’affaiblit. Ne vois-tu pas combien je suis frêle derrière ma carapace, que si je l’ôtais je deviendrais la proie du premier coup de dent ou de froid ?</p>
<p>» Si tu ne veux pas mourir avant d’avoir vécu, enlève cette carapace au plus vite, et ce sera ton chemin pour l’île. »</p>
<p>Comme la Tortue parlait, ma voix se voyait soudain alourdie et entravée par des vieux réflexes, des principes bancals, quelques rouages bloqués… Ces choses ne sont pas proprement siennes, mais il n’est pas toujours facile de s’en distinguer. Aussi, à mesure qu’elle les discerne, elle ne tarde pas à s’en séparer en les lâchant dans la mer.</p>
<p>Quelle pollution pour la mer, mais quelle légèreté pour ma voix ! Elle se sent une autre. Quel beau voyage !</p>
<p>Et ce voyage continue, sur le dos de la Tortue, au fil de l’eau bleue de la mer.</p>
<p>Ma voix pense. Elle pense à son parcours, à ses rencontres, au chemin qui lui reste. Elle pense à comment s’alléger encore. Elle pense à ce que lui a dit la Tortue. À ce qu’on lui a dit tout au long de son voyage. Elle pense à la Lune, au Soleil. Elle pense. Elle pense, elle pense, puis elle ne pense plus.</p>
<p>Elle ne pense plus, et, là où elle voyait la mer, voici la terre.</p>
<p>La terre.</p>
<p>La terre est petite.</p>
<p>Derrière la voix, la Tortue s’éloigne.</p>
<p>Sur la terre, quelqu’un attend. Son allure est familière à ma voix.</p>
<p>« Bonjour, je suis heureux de te voir ici.</p>
<p>– Bonjour, répond ma voix interdite, qui êtes-vous ?</p>
<p>– Je suis ton adversaire le plus coriace et ton partisan le plus fidèle. Je suis l’artisan de ta sérénité et celui qui attise tes écarts. Je suis ta voix et ton silence. Je suis lui. Je suis toi.</p>
<p>– Moi ?</p>
<p>– C’est ça. Je suis le Vent, je suis la Lune, je suis la Mer, je suis le Soleil. Je suis toi.</p>
<p>– Moi ? Le Vent ? La Lune ?</p>
<p>– Oui, je suis le monde. Je suis le voyage vers toi qui mène à elle. Je suis ton voyage vers elle qui mène à toi. Je suis un peu elle, je suis un peu lui. Je suis surtout toi. Mais qui est qui dans le théâtre de la vie ? Où finit un rôle et où commence l’autre ?</p>
<p>» Si tu es arrivée jusqu’à moi, tu n’es plus très loin d’elle.</p>
<p>– Et où est-elle, alors, cette île ? Cette fille ? »</p>
<p>Mais il n’y a plus personne qui veuille répondre à ma voix. Le Vent est là, mais il ne dit rien. Il y a la Mer aussi, qui se tait. Et la Lune. Et le Soleil. Et la Tortue. Mais personne ne répond.</p>
<p>Ma voix se retourne.</p>
<p>Elle se tait.</p>
<p>Elle te regarde.</p>
<p>Elle te regarde, danser sur les vagues. Danser et devenir demain.</p>
<p>L’automne. J’habite sur mon poêle à bois, et je fais au cœur de mon cœur des tas de feuilles mortes pour préparer les feux de l’hiver où tu reviendras. Le vent les emporte et en tapisse mon plancher, et moi je refais les tas défaits. Dans ce silence de l’automne et de mon célibat, je te parle. Parfois, le vent reprend mes paroles en écho et je crois qu’il m’appelle au dehors. Ou bien je crois entendre pleurer la lune. D’autres fois, je te parle et je crois que le soleil timide de l’automne me répond à la fenêtre. Je regarde vers lui. Je crois qu’il me parle de toi. Je veux lui répondre : rien, aucun son ne vient. Ma voix n’est plus là.</p>
<p>Alors je reprends ma pelle et mon balai, et je refais les tas défaits.</p>